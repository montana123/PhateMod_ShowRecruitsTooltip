﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhateModShowRecruitsTooltip
{
    public class Recruits
    {
        public string Name { get; set; }
        public bool canRecruit { get; set; }
        public int Tier { get; set; }

        public Recruits(string name, bool canRecruit, int tier)
        {
            this.Name = name;
            this.canRecruit = canRecruit;
            this.Tier = tier;
        }
    }
}
