﻿using HarmonyLib;
using System;
using System.Runtime.InteropServices;
using TaleWorlds.Core;
using TaleWorlds.Engine.Screens;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TaleWorlds.CampaignSystem;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core.ViewModelCollection;
using System.Collections.Generic;
using System.Linq;

namespace PhateModShowRecruitsTooltip
{
    [HarmonyPatch(typeof(TooltipVM), "OpenTooltip")]
    public class UpdateTooltipPatch
    {
        [HarmonyPostfix]
        private static void UpdateTooltipPostfix(TooltipVM __instance, Type type, object[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    if (args[0].GetType() == typeof(int) && (int)args[0] != 0)
                    {
                        PartyBase party = PartyBase.FindParty((int)args[0]);

                        if (party.IsSettlement)
                        {
                            List<Recruits> recruits = new List<Recruits>();

                            foreach (Hero sellerHero in party.Settlement.HeroesWithoutParty)
                            {
                                int maxIndex = Helpers.HeroHelper.MaximumIndexHeroCanRecruitFromHero(Hero.MainHero, sellerHero);
                                for (int i = 0; i < sellerHero.VolunteerTypes.Length; i++)
                                {
                                    CharacterObject character = sellerHero.VolunteerTypes[i];

                                    if (character == null)
                                        continue;

                                    Recruits recruit = new Recruits(character.Name.ToString(), i + 1 <= maxIndex, character.Tier);
                                    recruits.Add(recruit);
                                }
                            }
                            List<Tiermap> tierMap = new List<Tiermap>();
                            List<Tiermap> notRecruitableTierMap = new List<Tiermap>();

                            foreach (Recruits recruit in recruits)
                            {
                                if (recruit.canRecruit)
                                {
                                    if (tierMap.Where(x => x.Tier == recruit.Tier).FirstOrDefault() != null)
                                    {
                                        Tiermap tier = tierMap.Where(x => x.Tier == recruit.Tier).FirstOrDefault();
                                        if (tier != null)
                                            tier.count = tier.count + 1;
                                    }
                                    else
                                    {
                                        tierMap.Add(new Tiermap { count = 1, Tier = recruit.Tier });
                                    }
                                }
                                else
                                {
                                    if (notRecruitableTierMap.Where(x => x.Tier == recruit.Tier).FirstOrDefault() != null)
                                    {
                                        Tiermap tier = notRecruitableTierMap.Where(x => x.Tier == recruit.Tier).FirstOrDefault();
                                        if (tier != null)
                                            tier.count = tier.count + 1;
                                    }
                                    else
                                    {
                                        notRecruitableTierMap.Add(new Tiermap { count = 1, Tier = recruit.Tier });
                                    }
                                }
                            }

                            tierMap = tierMap.OrderBy(x => x.Tier).ToList();
                            notRecruitableTierMap = notRecruitableTierMap.OrderBy(x => x.Tier).ToList();
                            double sumRecruits = tierMap.Sum(item => item.count);
                            double sumNotRecruitableRecruits = notRecruitableTierMap.Sum(item => item.count);
                            if (tierMap.Count > 0)
                            {
                                __instance.TooltipPropertyList.Add(new TooltipProperty("", "Recruitable Recruits: " + sumRecruits, 0));
                            }
                            foreach (Tiermap tierM in tierMap)
                            {
                                __instance.TooltipPropertyList.Add(new TooltipProperty("", $"Tier: {tierM.Tier} - Troops: {tierM.count}", 0));
                            }

                            if (notRecruitableTierMap.Count > 0)
                            {
                                __instance.TooltipPropertyList.Add(new TooltipProperty("", "Nonrecruitable Recruits: " + sumNotRecruitableRecruits, 0));
                            }

                            foreach (Tiermap tierM in notRecruitableTierMap)
                            {
                                __instance.TooltipPropertyList.Add(new TooltipProperty("", $"Tier: {tierM.Tier} - Troops: {tierM.count}", 0));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}
